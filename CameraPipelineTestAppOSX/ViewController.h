//
//  ViewController.h
//  CameraPipelineTestAppOSX
//
//  Created by JiangZhiping on 16/3/24.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <OpencvCameraPipeline/OpencvCameraPipeline.h>

@interface ViewController : NSViewController

@property (nonatomic) OpencvProcessPipeline *pipeline;

@property (weak) IBOutlet NSUIView *previewerView;

- (IBAction)takePicture:(id)sender;
- (IBAction)startRecording:(id)sender;
- (IBAction)stopRecording:(id)sender;

@end

