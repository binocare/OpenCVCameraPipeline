//
//  main.m
//  CameraPipelineTestAppOSX
//
//  Created by JiangZhiping on 16/3/24.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
