//
//  ViewController.m
//  CameraPipelineTestAppOSX
//
//  Created by JiangZhiping on 16/3/24.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.pipeline = [[OpencvProcessPipeline alloc] initWithCameraPosition:AVCaptureDevicePositionFront];
}

- (void)viewDidAppear {
    [self.pipeline startRunning];
}

- (IBAction)takePicture:(id)sender {
    [self.pipeline takePicture];
}

- (IBAction)startRecording:(id)sender {
    [self.pipeline startRecording];
}
- (IBAction)stopRecording:(id)sender {
    [self.pipeline stopRecording];
}

@end
