//
//  CapturePipeLine.h
//  CameraPipeLine
//
//  Created by JiangZhiping on 15/10/23.
//  Copyright © 2015年 JiangZhiping. All rights reserved.
//

#import "AbstractProcessInput.h"
#import "OpenCVProcessingBridge.h"
#import <Photos/Photos.h>

@interface OpencvProcessPipeline : NSObject

@property (strong, nonatomic, nonnull) id<AbstractProcessInput> processInput;
@property (strong, nonatomic, nonnull) OpenCVProcessingBridge *processBridge;
@property (weak, nonatomic, nullable) NSUIView * previewRootView;
@property (nonatomic, readonly, getter = isPipelineRunning) BOOL pipelineRunning;

- (nonnull instancetype) initWithCameraPosition:(AVCaptureDevicePosition)initialCameraPosition NS_DESIGNATED_INITIALIZER;
- (nonnull instancetype) initWithVideoFilePath:(nonnull NSString *)filePath NS_DESIGNATED_INITIALIZER;


#pragma mark - actions
- (void) startRunning;
- (void) stopRunning;
- (void) takePicture;
- (void) startRecording;
- (void) stopRecording;
@end

