//
//  XplatformCameraPreviewer.m
//  OpenCVCameraPipeline
//
//  Created by JiangZhping on 2016/10/5.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "XplatformCameraPreviewer.h"

@implementation XplatformCameraPreviewer {
    NSTimer * initializationTimer;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self =[super initWithCoder:coder];
    if (self) {
        [self initJob];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frameRect {
    self = [super initWithFrame:frameRect];
    if (self) {
        [self initJob];
    }
    return self;
}

- (void) initJob {
    [NSObject retrieveObjectWithName:@"OpenCVProcessPipeline" AsyncWithBlock:^void (id retrievedObject) {
        [retrievedObject setValue:self forKey:@"previewRootView"];
    }];
}


@end
