//
//  ViewController.m
//  CameraPipelineTestAppiOS
//
//  Created by JiangZhiping on 16/3/19.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void) viewDidAppear:(BOOL)animated {
    CameraPipelineViewController * controller = [[CameraPipelineViewController alloc] initCameraPipelineViewController];
    
    [self presentViewController:controller animated:YES completion: ^{
    }];
}


@end
