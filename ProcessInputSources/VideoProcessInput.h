//
//  VideoProcessInput.h
//  OpenCVCameraPipeline
//
//  Created by JiangZhiping on 16/4/25.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "AbstractProcessInput.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreVideo/CoreVideo.h>

@interface VideoProcessInput : NSObject<AbstractProcessInput>

@property (nonatomic) NSURL * videoFileURL;

-(instancetype) initWithFilePath:(NSString *)videoFilePath;
-(instancetype) initWithVideoFileURL:(NSURL *)videoFileURL NS_DESIGNATED_INITIALIZER;

@end
