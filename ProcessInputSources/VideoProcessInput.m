//
//  VideoProcessInput.m
//  OpenCVCameraPipeline
//
//  Created by JiangZhiping on 16/4/25.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "VideoProcessInput.h"

@implementation VideoProcessInput {
    AVAsset * asset;
    AVAssetReader * assetReader;
    AVAssetTrack * videoTrack;
    AVAssetReaderTrackOutput * trackOutput;
    
    dispatch_queue_t videoTrackOutputQueue;
    NSUInteger frameCount;
}

@synthesize videoFileURL = _videoFileURL;
@synthesize delegate = _delegate;

- (instancetype) initWithVideoFileURL:(NSURL *)videoFileURL {
    self = [super init];
    if (self) {
        videoTrackOutputQueue = dispatch_queue_create("videoTrackOutputQueue", DISPATCH_QUEUE_SERIAL);
        self.videoFileURL = videoFileURL;
    }
    return self;
}

-(instancetype) initWithFilePath:(NSString *)videoFilePath {
    NSURL * url = [NSURL fileURLWithPath:videoFilePath];
    return [self initWithVideoFileURL:url];
}

- (void) setVideoFileURL:(NSURL *)videoFileURL {
    asset = [AVAsset assetWithURL:videoFileURL];
    NSArray * tracks = [asset tracksWithMediaType:AVMediaTypeVideo];
    if (tracks.count >0) {
        videoTrack = tracks[0];
    }

    trackOutput = [[AVAssetReaderTrackOutput alloc] initWithTrack:videoTrack outputSettings:@{ (NSString *)kCVPixelBufferPixelFormatTypeKey : [NSNumber numberWithInt:kCVPixelFormatType_32BGRA] }];
    
    assetReader = [[AVAssetReader alloc] initWithAsset:asset error:nil];
    [assetReader addOutput:trackOutput];
    
}

- (void) startRunning {
    frameCount = 0;
    [assetReader startReading];
    dispatch_async(videoTrackOutputQueue, ^{
        CMSampleBufferRef buffer = NULL;
        BOOL continueReading = YES;
        while (continueReading) {
            AVAssetReaderStatus status = assetReader.status;
            switch (status) {
                case AVAssetReaderStatusUnknown: {
                } break;
                case AVAssetReaderStatusReading: {
                    buffer = [trackOutput copyNextSampleBuffer];
                    
                    if (!buffer) {
                        break;
                    }
                    
                    CVPixelBufferRef rawPB = CMSampleBufferGetImageBuffer(buffer);
                    CMTime time = CMSampleBufferGetPresentationTimeStamp(buffer);
                    frameCount ++;
                    [self.delegate newPixelBufferDidReceive:rawPB AtTime:time FrameCount:frameCount];
                    
                } break;
                case AVAssetReaderStatusCompleted: {
                    continueReading = NO;
                } break;
                case AVAssetReaderStatusFailed: {
                    [assetReader cancelReading];
                    continueReading = NO;
                } break;
                case AVAssetReaderStatusCancelled: {
                    continueReading = NO;
                } break;
            }
            if (buffer) {
                CMSampleBufferInvalidate(buffer);
                CFRelease(buffer);
                buffer = NULL;
            }
        }
    });
}

- (void) stopRunning {
    
}

@end
