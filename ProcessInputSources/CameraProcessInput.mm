//
//  CameraProcessInput.m
//  OpenCVCameraPipeline
//
//  Created by JiangZhiping on 16/4/25.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "CameraProcessInput.h"
#import <CocoaCVUtilities/CocoaCVUtilities.h>
#import <Photos/Photos.h>

@implementation CameraProcessInput {
    AVCaptureDevicePosition _initialCameraPosition;
    dispatch_queue_t  _sessionQueue;
    dispatch_queue_t  _videoDataOutputQueue;
    AVCaptureStillImageOutput * _stillImageOutput;
    
    CMTime lastFrameTimestamp;
    NSUInteger frameCount;
}

@synthesize delegate = _delegate;

- (instancetype) initWithCameraPosition:(AVCaptureDevicePosition) initialCameraPosition {
    self = [super init];
    if (self) {
        _sessionQueue = dispatch_queue_create("CaptureSessionQueue", DISPATCH_QUEUE_SERIAL);
        _videoDataOutputQueue =dispatch_queue_create("VideoDataOutputQueue", DISPATCH_QUEUE_SERIAL);
        dispatch_set_target_queue( _videoDataOutputQueue, dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0 ) );
        
#if TARGET_OS_IPHONE
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:@"UIDeviceOrientationDidChangeNotification" object:nil];
        
        _initialCameraPosition = initialCameraPosition;
#endif
    }
    return self;
}

#if TARGET_OS_IPHONE
- (void)orientationChanged:(NSNotification *)notification{
    
    UIDeviceOrientation notifiedOrientation = [[UIDevice currentDevice] orientation];
    
    AVCaptureVideoOrientation orientationTobeSet = (AVCaptureVideoOrientation)notifiedOrientation;
    if (notifiedOrientation == UIDeviceOrientationFaceUp || notifiedOrientation == UIDeviceOrientationFaceDown) {
        orientationTobeSet = AVCaptureVideoOrientationLandscapeRight;
    }
    
    if (_videoConnection) {
        _videoOrientation = orientationTobeSet;
        [_videoConnection setVideoOrientation:orientationTobeSet];
    }
}
#endif

- (void) setupPipeline {
    if (_captureSession) {
        return;
    }
    
    _captureSession = [[AVCaptureSession alloc] init];
    _captureSession.sessionPreset = AVCaptureSessionPresetPhoto;
    
    _videoDevice = [AVCaptureDevice deviceWithPosition:_initialCameraPosition];
    
    AVCaptureDeviceInput *videoInput = [[AVCaptureDeviceInput alloc] initWithDevice:_videoDevice error:nil];
    if ([_captureSession canAddInput:videoInput]) {
        [_captureSession addInput:videoInput];
    }
    
    // setup videodata output
    _videoDataOutput = [[AVCaptureVideoDataOutput alloc] init];
    _videoDataOutput.videoSettings = @{ (id)kCVPixelBufferPixelFormatTypeKey : @(kCVPixelFormatType_32BGRA) };
    _videoDataOutput.alwaysDiscardsLateVideoFrames  = YES;
    
    [_videoDataOutput setSampleBufferDelegate:self queue:_videoDataOutputQueue];
    
    if ([_captureSession canAddOutput:_videoDataOutput]) {
        [_captureSession addOutput:_videoDataOutput];
    }
    _videoConnection = [_videoDataOutput connectionWithMediaType:AVMediaTypeVideo];
    
#if TARGET_OS_IPHONE
    [_videoDevice lockForConfiguration:nil];
    _videoDevice.activeFormat = [[_videoDevice formats]lastObject];
    [_videoDevice unlockForConfiguration];
#endif
    
    // setup stillimageoutput
    _stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    if ([_captureSession canAddOutput:_stillImageOutput]) {
        _stillImageOutput.outputSettings = @{AVVideoCodecKey : AVVideoCodecJPEG};
        [_captureSession addOutput:_stillImageOutput];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UIDeviceOrientationDidChangeNotification" object:nil];
}

- (void) startRunning {
    frameCount = 0;
    dispatch_async(_sessionQueue, ^{
        [self setupPipeline];
        [_captureSession startRunning];
    });
}

- (void) stopRunning {
    if (!_captureSession.running) {
        return;
    }
    dispatch_async(_sessionQueue, ^{
        [self setupPipeline];
        [_captureSession stopRunning];
    });
}

-(void) takePicture {
    dispatch_async(_sessionQueue, ^{
        AVCaptureConnection *connection = [_stillImageOutput connectionWithMediaType:AVMediaTypeVideo];
        connection.videoOrientation = _videoOrientation;
        [_stillImageOutput captureStillImageAsynchronouslyFromConnection:connection completionHandler:^( CMSampleBufferRef imageDataSampleBuffer, NSError *error ) {
            if (imageDataSampleBuffer) {
                NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString * documentDirPath = paths[0];
                
                NSString *temporaryFileName = [NSProcessInfo processInfo].globallyUniqueString;
                NSString * filePath = [documentDirPath stringByAppendingPathComponent:[temporaryFileName stringByAppendingPathExtension:@"jpg"]];
                
                NSLog(@"%d",[[NSFileManager defaultManager] fileExistsAtPath:filePath]);
                NSError * error;
                [imageData writeToFile:filePath options:NSDataWritingAtomic error:&error];
                if (error) {
                    NSLog(@"%@",error);
                }
            }
            
        }];
    });
}

- (void) captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
    CVPixelBufferRef rawPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    frameCount ++;
    lastFrameTimestamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
    [self.delegate newPixelBufferDidReceive:rawPixelBuffer AtTime:lastFrameTimestamp FrameCount:frameCount];
}


@end
