//
//  CameraProcessInputWithSettings.m
//  OpenCVCameraPipeline
//
//  Created by JiangZhiping on 16/4/25.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "CameraProcessInputWithSettings.h"

@implementation CameraProcessInputWithSettings

- (void) setupPipeline {
    [super setupPipeline];
    _cameraPosition = self.videoDevice.position;
    _focusMode = self.videoDevice.focusMode;
    _exposureMode = self.videoDevice.exposureMode;
    _whiteBalanceMode = self.videoDevice.whiteBalanceMode;
    
#if TARGET_OS_IPHONE
    _lensPosition = self.videoDevice.lensPosition;
    _exposureISO = self.videoDevice.ISO;
    _exposureDuration = CMTimeGetSeconds(self.videoDevice.exposureDuration);
#endif
}

- (void) setCameraPosition:(AVCaptureDevicePosition)aCameraPosition {
    if (self.cameraPosition != aCameraPosition) {
        [self.captureSession beginConfiguration];
        
        for (AVCaptureDeviceInput * input in (self.captureSession).inputs) {
            [self.captureSession removeInput:input];
        }
        
        self.videoDevice = [CameraProcessInputWithSettings getCameraAccordingToPosition:aCameraPosition];
        AVCaptureDeviceInput *newInput = [[AVCaptureDeviceInput alloc] initWithDevice:self.videoDevice error:nil];
        
        if ([self.captureSession canAddInput:newInput]) {
            [self.captureSession addInput:newInput];
            
            _cameraPosition = aCameraPosition;
        }
        
        
        self.videoConnection = [self.videoDataOutput connectionWithMediaType:AVMediaTypeVideo];
        
        [self.captureSession commitConfiguration];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UIDeviceOrientationDidChangeNotification" object:nil];
    }
}

+ (AVCaptureDevice *) getCameraAccordingToPosition:(AVCaptureDevicePosition)position {
    NSArray * availableCameras = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    if (availableCameras.count == 1) {
        // on MAC OS X, the ONLY front facing camera has no 'position' property.
        return availableCameras[0];
    }
    for (AVCaptureDevice * device in availableCameras) {
        if (device.position == position) {
            return device;
        }
    }
    return nil;
}


- (void) setFocusMode:(AVCaptureFocusMode)focusMode {
    NSError *error = nil;
    if ([self.videoDevice lockForConfiguration:&error]) {
        if ([self.videoDevice isFocusModeSupported:focusMode]) {
            self.videoDevice.focusMode = focusMode;
            _focusMode = focusMode;
        } else {
            NSLog(@"Focus mode can not be set.");
        }
        [self.videoDevice unlockForConfiguration];
        
    } else {
        NSLog(@"Can not lock device");
    }
}

- (void) tapToFocusToPoint:(CGPoint)devicePoint {
    NSError *error = nil;
    if ([self.videoDevice lockForConfiguration:&error]) {
        if ((self.videoDevice).focusPointOfInterestSupported) {
            self.videoDevice.focusPointOfInterest = devicePoint;
        } else {
            NSLog(@"FocusPointOfInterest can not be set.");
        }
        [self.videoDevice unlockForConfiguration];
        
    } else {
        NSLog(@"Can not lock device");
    }
}

- (void) setExposureMode:(AVCaptureExposureMode)exposureMode {
    NSError * error = nil;
    if ([self.videoDevice lockForConfiguration:&error]) {
        if ([self.videoDevice isExposureModeSupported:exposureMode]) {
            self.videoDevice.exposureMode = exposureMode;
            _exposureMode = exposureMode;
        } else {
            NSLog(@"Exposure Mode %ld is not supported. Current focus mode is %ld",(long)exposureMode,(long)self.videoDevice.exposureMode);
        }
        [self.videoDevice unlockForConfiguration];
        
    } else {
        NSLog(@"Can not lock device");
    }
}

- (void) setWhiteBalanceMode:(AVCaptureWhiteBalanceMode)whiteBalanceMode {
    NSError * error = nil;
    if ([self.videoDevice lockForConfiguration:&error]) {
        if ([self.videoDevice isWhiteBalanceModeSupported:whiteBalanceMode]) {
            self.videoDevice.whiteBalanceMode = whiteBalanceMode;
            _whiteBalanceMode = whiteBalanceMode;
        } else {
            NSLog(@"White Balance Mode %ld is not supported. Current white balance mode is %ld.",(long)whiteBalanceMode,(long)self.videoDevice.whiteBalanceMode);
        }
        [self.videoDevice unlockForConfiguration];
        
    } else {
        NSLog(@"Can not lock device");
    }
}

#if TARGET_OS_IPHONE

//+ (void) fillPipeline:(CameraProcessInputWithSettings *) pipeline toTheSettingsPanelSegue: (UIStoryboardSegue *) segue{
//    UINavigationController *navController = [segue destinationViewController];
//    SettingsPortalTableViewController *settingController = (SettingsPortalTableViewController *)navController.topViewController;
//    settingController.pipeline = _delegate;
//    settingController.cameraProcessInputWithSettings = self;
//}

- (void) setSmoothAutoFocusEnabled:(BOOL)aSmoothAutoFocusEnabled {
    NSError *error = nil;
    if ([self.videoDevice lockForConfiguration:&error]) {
        if ([self.videoDevice isSmoothAutoFocusSupported]) {
            self.videoDevice.smoothAutoFocusEnabled = aSmoothAutoFocusEnabled;
            _smoothAutoFocusEnabled = aSmoothAutoFocusEnabled;
        } else {
            NSLog(@"Smooth auto focus mode can not be set.");
        }
        [self.videoDevice unlockForConfiguration];
        
    } else {
        NSLog(@"Can not lock device");
    }
}



- (void) setLensPosition:(float)lensPosition {
    NSError * error = nil;
    if ([self.videoDevice lockForConfiguration:&error]) {
        if (self.videoDevice.focusMode == AVCaptureFocusModeLocked) {
            [self.videoDevice setFocusModeLockedWithLensPosition:lensPosition completionHandler:nil];
        }
        [self.videoDevice unlockForConfiguration];
        _lensPosition = lensPosition;
    } else {
        NSLog(@"Can not lock device");
    }
}

- (void) setExposureDuration:(float)exposureDuration {
    NSError * error = nil;
    if ([self.videoDevice lockForConfiguration:&error]) {
        if (self.videoDevice.exposureMode == AVCaptureExposureModeCustom) {
            [self.videoDevice setExposureModeCustomWithDuration:CMTimeMakeWithSeconds(exposureDuration, 1000*1000*1000) ISO:AVCaptureISOCurrent completionHandler:nil];
        }
        [self.videoDevice unlockForConfiguration];
        _exposureDuration = exposureDuration;
    } else {
        NSLog(@"Can not lock device");
    }
}

- (void) setExposureISO:(float)exposureISO {
    NSError * error = nil;
    if ([self.videoDevice lockForConfiguration:&error]) {
        if (self.videoDevice.exposureMode == AVCaptureExposureModeCustom) {
            [self.videoDevice setExposureModeCustomWithDuration:AVCaptureExposureDurationCurrent ISO:exposureISO completionHandler:nil];
        }
        [self.videoDevice unlockForConfiguration];
        _exposureISO = exposureISO;
    } else {
        NSLog(@"Can not lock device");
    }
}

- (void) setWhiteBalanceTemperature:(float)whiteBalanceTemperature {
    
    AVCaptureWhiteBalanceTemperatureAndTintValues previousTemperaturAndTint = [self.videoDevice temperatureAndTintValuesForDeviceWhiteBalanceGains:[self.videoDevice deviceWhiteBalanceGains]];
    
    AVCaptureWhiteBalanceTemperatureAndTintValues temperatureAndTint = {
        .temperature = whiteBalanceTemperature,
        .tint = previousTemperaturAndTint.tint,
    };
    
    AVCaptureWhiteBalanceGains correspondingGain = [self.videoDevice deviceWhiteBalanceGainsForTemperatureAndTintValues:temperatureAndTint];
    correspondingGain = [self normalizedGains:correspondingGain];
    
    NSError * error = nil;
    if ([self.videoDevice lockForConfiguration:&error]) {
        if (self.videoDevice.whiteBalanceMode == AVCaptureWhiteBalanceModeLocked) {
            [self.videoDevice setWhiteBalanceModeLockedWithDeviceWhiteBalanceGains:correspondingGain completionHandler:nil];
        }
        [self.videoDevice unlockForConfiguration];
        _whiteBalanceTemperature = whiteBalanceTemperature;
    } else {
        NSLog(@"Can not lock device");
    }
}

- (void) setWhiteBalanceTint:(float)whiteBalanceTint{
    
    AVCaptureWhiteBalanceTemperatureAndTintValues previousTemperaturAndTint = [self.videoDevice temperatureAndTintValuesForDeviceWhiteBalanceGains:[self.videoDevice deviceWhiteBalanceGains]];
    
    AVCaptureWhiteBalanceTemperatureAndTintValues temperatureAndTint = {
        .temperature = previousTemperaturAndTint.temperature,
        .tint = whiteBalanceTint,
    };
    
    AVCaptureWhiteBalanceGains correspondingGain = [self.videoDevice deviceWhiteBalanceGainsForTemperatureAndTintValues:temperatureAndTint];
    correspondingGain = [self normalizedGains:correspondingGain];
    
    NSError * error = nil;
    if ([self.videoDevice lockForConfiguration:&error]) {
        if (self.videoDevice.whiteBalanceMode == AVCaptureWhiteBalanceModeLocked) {
            [self.videoDevice setWhiteBalanceModeLockedWithDeviceWhiteBalanceGains:correspondingGain completionHandler:nil];
        }
        [self.videoDevice unlockForConfiguration];
        _whiteBalanceTint = whiteBalanceTint;
    } else {
        NSLog(@"Can not lock device");
    }
}

- (AVCaptureWhiteBalanceGains)normalizedGains:(AVCaptureWhiteBalanceGains) gains
{
    AVCaptureWhiteBalanceGains g = gains;
    
    g.redGain = MAX( 1.0, g.redGain );
    g.greenGain = MAX( 1.0, g.greenGain );
    g.blueGain = MAX( 1.0, g.blueGain );
    
    g.redGain = MIN( self.videoDevice.maxWhiteBalanceGain, g.redGain );
    g.greenGain = MIN( self.videoDevice.maxWhiteBalanceGain, g.greenGain );
    g.blueGain = MIN( self.videoDevice.maxWhiteBalanceGain, g.blueGain );
    
    return g;
}

#endif


@end
