//
//  CameraProcessInput.h
//  OpenCVCameraPipeline
//
//  Created by JiangZhiping on 16/4/25.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "AbstractProcessInput.h"
#import <AVFoundation/AVFoundation.h>

@interface CameraProcessInput :NSObject<AbstractProcessInput,AVCaptureVideoDataOutputSampleBufferDelegate>

@property (nonatomic) AVCaptureVideoOrientation  videoOrientation;

@property (nonatomic) AVCaptureSession * captureSession;
@property (nonatomic) AVCaptureDevice * videoDevice;
@property (nonatomic) AVCaptureConnection * videoConnection;
@property (nonatomic) AVCaptureVideoDataOutput * videoDataOutput;

- (instancetype) initWithCameraPosition:(AVCaptureDevicePosition) initialCameraPosition NS_DESIGNATED_INITIALIZER;

- (void) setupPipeline;

@end
