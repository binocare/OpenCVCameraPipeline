//
//  OpenCVProcessingBridge.h
//  CameraPipeLine
//
//  Created by JiangZhiping on 15/11/3.
//  Copyright © 2015年 JiangZhiping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreVideo/CoreVideo.h>
#import <CoreGraphics/CoreGraphics.h>
#import <opencv2/opencv.hpp>
#import <CocoaCVUtilities/CocoaCVUtilities.h>

@protocol OpenCVProcessDelegate;

@interface OpenCVProcessingBridge : NSObject

@property (nonatomic) NSMutableArray<id<OpenCVProcessDelegate>> * cvDelegates;
@property (nonatomic) NSMutableArray<id<OpenCVProcessDelegate>> * delegateDependencies;

+ (instancetype) getInstance;

- (void) addCVDelegate:(id<OpenCVProcessDelegate>) aCVDelegate;
- (void) addCVDelegate:(id<OpenCVProcessDelegate>) aCVDelegate withDependency:(id<OpenCVProcessDelegate>) dependentDelegate;

- (id) process:(CVPixelBufferRef)pixelBuffer;

@end

@protocol OpenCVProcessDelegate <NSObject>

@property (nonatomic) NSUInteger frameCount;
@property (nonatomic) CVPixelBufferRef pixelBuffer;
@property (nonatomic) cv::Mat & inputMat;
@property (nonatomic) cv::Mat & resultMat;
@property (nonatomic) BOOL drawResult;

+ (instancetype) getInstance;

- (void) process;
- (void) drawOnResultImage;
- (void) showSettings;

@end

