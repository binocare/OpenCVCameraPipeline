//
//  AbstractProcessDelegate.m
//  OpenCVCameraPipeline
//
//  Created by JiangZhiping on 16/3/26.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "AbstractProcessDelegate.h"

@implementation AbstractProcessDelegate

@synthesize readyToUse;
@synthesize frameCount;
@synthesize pixelBuffer;
@synthesize inputMat;
@synthesize resultMat;
@synthesize drawResult;

// Dictionary that holds all instances of subclasses
static NSMutableDictionary *_sharedInstances = nil;

+ (instancetype)getInstance
{
    id<OpenCVProcessDelegate> sharedInstance = nil;
    @synchronized(self)
    {
        NSString *instanceClass = NSStringFromClass(self);
        
        if (_sharedInstances == nil)
            _sharedInstances = [NSMutableDictionary dictionary];
        
        // Looking for existing instance
        sharedInstance = _sharedInstances[instanceClass];
        
        // If there's no instance – create one and add it to the dictionary
        if (sharedInstance == nil)
        {
            sharedInstance = [[super allocWithZone:nil] init];
            // default to enable
            _sharedInstances[instanceClass] = sharedInstance;
        }
    }
    return sharedInstance;
}

+ (NSMutableDictionary *) settingsDictionary {
    static NSMutableDictionary * dictionary = nil;
    if (!dictionary) {
        dictionary = [[NSMutableDictionary alloc] init];
    }
    return dictionary;
}

- (void) process{
}

- (void) drawOnResultImage {

}

- (void) debugDraw:(const cv::Mat &) aMat WithWindowName:(NSString *)windowName{
#if !TARGET_OS_IPHONE
    dispatch_sync(dispatch_get_main_queue(), ^{
        cv::namedWindow(windowName.UTF8String, cv::WINDOW_NORMAL || cv::WINDOW_KEEPRATIO);
        cv::imshow(windowName.UTF8String, aMat);
        cv::waitKey(1);
    });
#else 
    
#endif
}

- (void) showSettings {
    
}

@end
