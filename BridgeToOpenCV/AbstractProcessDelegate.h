//
//  AbstractProcessDelegate.h
//  OpenCVCameraPipeline
//
//  Created by JiangZhiping on 16/3/26.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OpenCVProcessingBridge.h"

@interface AbstractProcessDelegate : NSObject <OpenCVProcessDelegate>

@property (nonatomic, getter=isReadyToUse) BOOL readyToUse;

+ (NSMutableDictionary *) settingsDictionary;
- (void) debugDraw:(const cv::Mat &) aMat WithWindowName:(NSString *)windowName;

@end
