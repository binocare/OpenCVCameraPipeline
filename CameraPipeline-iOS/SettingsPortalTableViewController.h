//
//  SettingsTableViewController.h
//  GazeEstimationOnCocoa
//
//  Created by JiangZhiping on 15/11/21.
//  Copyright © 2015年 JiangZhiping. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpencvProcessPipeline.h"
#import "CameraProcessInputWithSettings.h"

@interface SettingsPortalTableViewController : UITableViewController

#pragma mark - generals

@property (weak, nonatomic) OpencvProcessPipeline * pipeline;
@property (weak,nonatomic) CameraProcessInputWithSettings * cameraProcessInputWithSettings;

@property (strong, nonatomic) IBOutlet UITableView *settingsHubTableView;
- (IBAction)settingDone:(id)sender;




@end
