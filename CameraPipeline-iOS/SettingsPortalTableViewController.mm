//
//  SettingsTableViewController.m
//  GazeEstimationOnCocoa
//
//  Created by JiangZhiping on 15/11/21.
//  Copyright © 2015年 JiangZhiping. All rights reserved.
//

#import "SettingsPortalTableViewController.h"
#import "PipelineSettingsTableViewController.h"
#import "CameraSettingsTableViewController.h"

@interface SettingsPortalTableViewController ()

@end

@implementation SettingsPortalTableViewController

- (IBAction)settingDone:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.row == 0) {
        [self performSegueWithIdentifier:@"ShowCapturePipelineSettings" sender:self];
    } else if (indexPath.row == 1) {
        [self performSegueWithIdentifier:@"ShowCameraSettings" sender:self];
    }
}


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"ShowCapturePipelineSettings"]) {
        PipelineSettingsTableViewController * view = (PipelineSettingsTableViewController *) segue.destinationViewController;
        view.pipeline = self.pipeline;
    } else if ([segue.identifier isEqualToString:@"ShowCameraSettings"]) {
        CameraSettingsTableViewController * view = (CameraSettingsTableViewController *) segue.destinationViewController;
        view.pipelineWithSettings = self.cameraProcessInputWithSettings;
    }
}


@end
