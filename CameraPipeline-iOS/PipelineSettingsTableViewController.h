//
//  PipelineSettingsTableViewController.h
//  GazeEstimationOnCocoa
//
//  Created by JiangZhiping on 15/11/21.
//  Copyright © 2015年 JiangZhiping. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpencvProcessPipeline.h"

@interface PipelineSettingsTableViewController : UITableViewController

#pragma mark - generals

@property (weak,nonatomic) OpencvProcessPipeline * pipeline;


@property (nonatomic) NSArray * outputFormats;
@property (nonatomic) NSArray * previewMethods;

#pragma mark - Pipeline Settings

@property (weak, nonatomic) IBOutlet UISegmentedControl *outputFormatSC;
@property (weak, nonatomic) IBOutlet UISegmentedControl *previewerMethodSC;

- (IBAction)changeOutputFormat:(id)sender;

- (IBAction)changePreviewerMethod:(id)sender;

@end
