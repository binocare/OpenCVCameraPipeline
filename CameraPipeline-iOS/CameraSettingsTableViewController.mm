//
//  CameraSettingsTableViewController.m
//  GazeEstimationOnCocoa
//
//  Created by JiangZhiping on 15/11/21.
//  Copyright © 2015年 JiangZhiping. All rights reserved.
//

#import "CameraSettingsTableViewController.h"

static void * lensPositionContext = & lensPositionContext;
static void * durationContext = & durationContext;
static void * isoContext = & isoContext;
static void * whiteBalanceGainsContext = & whiteBalanceGainsContext;

@interface CameraSettingsTableViewController ()

@end

@implementation CameraSettingsTableViewController {
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
    [self addObservers];
}

- (void) viewWillDisappear:(BOOL)animated {
    [self removeObservers];
}

- (void) addObservers {
    [self addObserver:self forKeyPath:@"pipelineWithSettings.videoDevice.lensPosition" options:NSKeyValueObservingOptionNew context:lensPositionContext];
    [self addObserver:self forKeyPath:@"pipelineWithSettings.videoDevice.exposureDuration" options:NSKeyValueObservingOptionNew context:durationContext];
    [self addObserver:self forKeyPath:@"pipelineWithSettings.videoDevice.ISO" options:NSKeyValueObservingOptionNew context:isoContext];
    [self addObserver:self forKeyPath:@"pipelineWithSettings.videoDevice.deviceWhiteBalanceGains" options:NSKeyValueObservingOptionNew context:whiteBalanceGainsContext];
}

- (void) removeObservers {
    [self removeObserver:self forKeyPath:@"pipelineWithSettings.videoDevice.lensPosition" context:lensPositionContext];
    [self removeObserver:self forKeyPath:@"pipelineWithSettings.videoDevice.exposureDuration" context:durationContext];
    [self removeObserver:self forKeyPath:@"pipelineWithSettings.videoDevice.ISO" context:isoContext];
    [self removeObserver:self forKeyPath:@"pipelineWithSettings.videoDevice.deviceWhiteBalanceGains" context:whiteBalanceGainsContext];
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    
//    id oldValue = change[NSKeyValueChangeOldKey];
    id newValue = change[NSKeyValueChangeNewKey];
    
    if (!newValue || newValue == [NSNull null])
        return;
    
    if (context == lensPositionContext) {
        _lensPositionSlider.value = [newValue floatValue];
        _lensPositionLabel.text = [NSString stringWithFormat:@"%.2f",[newValue floatValue]];
    } else if (context == durationContext) {
        if (_pipelineWithSettings.exposureMode != AVCaptureExposureModeCustom) {
            double duration = CMTimeGetSeconds([newValue CMTimeValue]);
            _durationSlider.value = duration;
            _durationLabel.text = [NSString stringWithFormat:@"%.2f",duration];
        }
    } else if (context == isoContext) {
        if (_pipelineWithSettings.exposureMode != AVCaptureExposureModeLocked) {
            _isoSlider.value = [newValue floatValue];
            _isoLabel.text = [NSString stringWithFormat:@"%d",[newValue intValue]];
        }
    } else if (context == whiteBalanceGainsContext) {
        if (_pipelineWithSettings.whiteBalanceMode != AVCaptureWhiteBalanceModeLocked) {
            AVCaptureWhiteBalanceTemperatureAndTintValues TandT = [_pipelineWithSettings.videoDevice temperatureAndTintValuesForDeviceWhiteBalanceGains:[_pipelineWithSettings.videoDevice deviceWhiteBalanceGains]];
            _whiteBalanceTemperatureSlider.value = TandT.temperature;
            _whiteBalanceTemperatureLabel.text = [NSString stringWithFormat:@"%.1f",TandT.temperature];
            _whiteBalanceTintSlider.value = TandT.tint;
            _whiteBalanceTintLabel.text = [NSString stringWithFormat:@"%.1f",TandT.tint];
        }
    }
}

- (void) setup {
    _discardLateFramePolicySwitch.on = _pipelineWithSettings.videoDataOutput.alwaysDiscardsLateVideoFrames;
    
    // camera position
    _cameraPositions = @[@(AVCaptureDevicePositionFront), @(AVCaptureDevicePositionBack)];
    _cameraPositionSC.selectedSegmentIndex = [self.cameraPositions indexOfObject:@(_pipelineWithSettings.cameraPosition)];
    
    // focus mode
    _focusModes = @[@(AVCaptureFocusModeLocked), @(AVCaptureFocusModeAutoFocus), @(AVCaptureFocusModeContinuousAutoFocus)];
    _focusRanges = @[@(AVCaptureAutoFocusRangeRestrictionNear), @(AVCaptureAutoFocusRangeRestrictionFar),@(AVCaptureAutoFocusRangeRestrictionNone)];
    _focusModeSC.enabled = (_pipelineWithSettings.videoDevice != nil);
    _focusModeSC.selectedSegmentIndex = [self.focusModes indexOfObject:@(_pipelineWithSettings.focusMode)];
    for (NSNumber * mode in self.focusModes) {
        [self.focusModeSC setEnabled:([_pipelineWithSettings.videoDevice isFocusModeSupported:(AVCaptureFocusMode)mode.intValue]) forSegmentAtIndex:[self.focusModes indexOfObject:mode]];
    }
    
    _smoothedAFSwitch.enabled = ([_pipelineWithSettings.videoDevice isSmoothAutoFocusSupported]);
    _smoothedAFSwitch.on = ([_pipelineWithSettings.videoDevice isSmoothAutoFocusEnabled]);

    _lensPositionSlider.enabled = (_pipelineWithSettings.focusMode == AVCaptureFocusModeLocked && [_pipelineWithSettings.videoDevice isFocusModeSupported:AVCaptureFocusModeLocked]);
    
    
    // exposure mode
    _exposureModes = @[@(AVCaptureExposureModeLocked),@(AVCaptureExposureModeAutoExpose), @(AVCaptureExposureModeContinuousAutoExposure), @(AVCaptureExposureModeCustom)];

    _exposureModeSC.selectedSegmentIndex = [self.exposureModes indexOfObject:@(_pipelineWithSettings.exposureMode)];
    for (NSNumber * mode in self.exposureModes) {
        [_exposureModeSC setEnabled:([_pipelineWithSettings.videoDevice isExposureModeSupported:(AVCaptureExposureMode)mode.intValue]) forSegmentAtIndex:[self.exposureModes indexOfObject:mode]];
    }
    
    _durationSlider.enabled = (_pipelineWithSettings.exposureMode == AVCaptureExposureModeCustom);
    _durationSlider.minimumValue = CMTimeGetSeconds(_pipelineWithSettings.videoDevice.activeFormat.minExposureDuration);
    _durationSlider.maximumValue = CMTimeGetSeconds(_pipelineWithSettings.videoDevice.activeFormat.maxExposureDuration);
    
    _isoSlider.enabled = (_pipelineWithSettings.exposureMode == AVCaptureExposureModeCustom);
    _isoSlider.minimumValue = _pipelineWithSettings.videoDevice.activeFormat.minISO;
    _isoSlider.maximumValue = _pipelineWithSettings.videoDevice.activeFormat.maxISO;
    
    // whitebalance
    _whiteBalanceModes = @[@(AVCaptureWhiteBalanceModeLocked),@(AVCaptureWhiteBalanceModeAutoWhiteBalance),@(AVCaptureWhiteBalanceModeContinuousAutoWhiteBalance)];
    _whiteBalanceModeSC.enabled = (_pipelineWithSettings.videoDevice != nil);
    _whiteBalanceModeSC.selectedSegmentIndex = [self.whiteBalanceModes indexOfObject:@(_pipelineWithSettings.videoDevice.whiteBalanceMode)];
    for (NSNumber * mode in self.whiteBalanceModes) {
        [_whiteBalanceModeSC setEnabled:([_pipelineWithSettings.videoDevice isWhiteBalanceModeSupported:(AVCaptureWhiteBalanceMode)mode.intValue]) forSegmentAtIndex:[self.whiteBalanceModes indexOfObject:mode]];
    }
    
    _whiteBalanceTemperatureSlider.minimumValue = 3000;
    _whiteBalanceTemperatureSlider.maximumValue = 8000;
    _whiteBalanceTintSlider.minimumValue = -150;
    _whiteBalanceTintSlider.maximumValue = 150;
}

- (IBAction)changeDiscardLateFramePolicy:(id)sender {
    _pipelineWithSettings.videoDataOutput.alwaysDiscardsLateVideoFrames = _discardLateFramePolicySwitch.on;
}

- (IBAction)changeCameraPosition:(id)sender {
    
    _pipelineWithSettings.cameraPosition = (AVCaptureDevicePosition)[[self.cameraPositions objectAtIndex:_cameraPositionSC.selectedSegmentIndex] shortValue];
}

- (IBAction)changeFocusMode:(id)sender {
    _pipelineWithSettings.focusMode = (AVCaptureFocusMode)[[_focusModes objectAtIndex:_focusModeSC.selectedSegmentIndex] intValue];
    
    _lensPositionSlider.enabled = (_pipelineWithSettings.focusMode == AVCaptureFocusModeLocked);
}

- (IBAction)changeLensPosition:(id)sender {
    _pipelineWithSettings.lensPosition = _lensPositionSlider.value;
}

- (IBAction)changeSmoothedAF:(id)sender {
    _pipelineWithSettings.smoothAutoFocusEnabled = _smoothedAFSwitch.on;
}

- (IBAction)changeExposureMode:(id)sender {
    _pipelineWithSettings.exposureMode = (AVCaptureExposureMode)[[_exposureModes objectAtIndex:_exposureModeSC.selectedSegmentIndex] intValue];
    _durationSlider.enabled = (_pipelineWithSettings.exposureMode == AVCaptureExposureModeCustom);
    _isoSlider.enabled = (_pipelineWithSettings.exposureMode == AVCaptureExposureModeCustom);
}

- (IBAction)changeDuration:(id)sender {
    _durationLabel.text = [NSString stringWithFormat:@"%.2f",_durationSlider.value];
    _pipelineWithSettings.exposureDuration = _durationSlider.value;
}

- (IBAction)changeISO:(id)sender {
    _isoLabel.text = [NSString stringWithFormat:@"%.2f",_isoSlider.value];
    _pipelineWithSettings.exposureISO = _isoSlider.value;
}

- (IBAction)changeWhiteBalanceMode:(id)sender {
    _pipelineWithSettings.whiteBalanceMode = (AVCaptureWhiteBalanceMode)[[_whiteBalanceModes objectAtIndex:_whiteBalanceModeSC.selectedSegmentIndex] intValue];
}

- (IBAction)changeWhiteBalanceTemperature:(id)sender {
    _whiteBalanceTemperatureLabel.text = [NSString stringWithFormat:@"%.2f",_whiteBalanceTemperatureSlider.value];
    _pipelineWithSettings.whiteBalanceTemperature = _whiteBalanceTemperatureSlider.value;
}

- (IBAction)changeWhiteBalanceTint:(id)sender {
    _whiteBalanceTintLabel.text = [NSString stringWithFormat:@"%.2f",_whiteBalanceTintSlider.value];
    _pipelineWithSettings.whiteBalanceTint = _whiteBalanceTintSlider.value;
}

@end
