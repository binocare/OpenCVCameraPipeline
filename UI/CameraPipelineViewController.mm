//
//  CameraPipelineViewController.m
//  OpenCVCameraPipeline
//
//  Created by JiangZhiping on 16/5/6.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "CameraPipelineViewController.h"

@implementation CameraPipelineViewController {
    
}

- (instancetype) initCameraPipelineViewController {
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"CameraPipelineView" bundle:[NSBundle bundleForClass:CameraPipelineViewController.class]];
    CameraPipelineViewController * vc = (CameraPipelineViewController* ) [storyboard instantiateViewControllerWithIdentifier:@"CameraPipelineViewController"];

    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.pipeline = [[OpencvProcessPipeline alloc] initWithCameraPosition:AVCaptureDevicePositionFront];
    
    [self setShowDoneButton:YES];
    
    if (![self isDoneButtonShown]) {
        [_doneButton setHidden:YES];
    }
}

#if TARGET_OS_IPHONE
- (void) viewDidAppear:(BOOL)animated {
#else
- (void) viewDidAppear: {
#endif
    [super viewDidAppear:animated];
    [_pipeline startRunning];
}

- (IBAction)takePicture:(UIButton *)sender {
    [_pipeline takePicture];
}

- (IBAction)changeCameraDirection:(UIButton *)sender {
    CameraProcessInputWithSettings * cameraInput = (CameraProcessInputWithSettings *) self.pipeline.processInput;
    if (cameraInput.cameraPosition == AVCaptureDevicePositionBack) {
        cameraInput.cameraPosition = AVCaptureDevicePositionFront;
    } else if (cameraInput.cameraPosition == AVCaptureDevicePositionFront) {
        cameraInput.cameraPosition = AVCaptureDevicePositionBack;
    }
}

- (IBAction)recordOrStopVideo:(UIButton *)sender {
    if ([self isRecording]) {
        [_pipeline stopRecording];
        [sender setTitle:@"Record" forState:UIControlStateNormal];
        _recording = NO;
    } else {
        [_pipeline startRecording];
        [sender setTitle:@"Stop" forState:UIControlStateNormal];
        _recording = YES;
    }
}
- (IBAction)done:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}
@end
