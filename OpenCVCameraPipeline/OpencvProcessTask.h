//
//  OpencvProcessTask.hpp
//  OpenCVCameraPipeline
//
//  Created by JiangZhping on 2017/1/16.
//  Copyright © 2017年 JiangZhiping. All rights reserved.
//

#ifndef OpencvProcessTask_hpp
#define OpencvProcessTask_hpp

#include <opencv2/opencv.hpp>

class OpencvProcessTask {
public:
    cv::Mat inputMat;
    cv::Mat canvasMat;
    bool drawResult; // read-only
    long frameCount;
    
    virtual void process() = 0;
    virtual void drawOnResultImage() = 0;
};

#endif /* OpencvProcessTask_hpp */
